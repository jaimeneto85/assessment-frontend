const webpack = require('webpack');

module.exports = {
  entry: './assets/scripts/index.js',
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      },
      {
	    test: /\.scss$/,
	    use: [
	        'style-loader',
	        'css-loader',
	        'sass-loader',
	        {
	          loader: 'sass-resources-loader',
	          options: {
	            resources: './assets/style/base.scss',
	          },
	        },
	      ],
	    },
    ],
  },
  resolve: {
    extensions: ['*', '.js', '.jsx']
  },
  output: {
    path: __dirname + '/public',
    publicPath: '/',
    filename: 'bundle.js'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    contentBase: './public',
    hot: true
  }
};