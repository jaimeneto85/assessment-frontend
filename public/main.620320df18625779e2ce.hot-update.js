webpackHotUpdate("main",{

/***/ "./assets/scripts/containers/ProductsList.jsx":
/*!****************************************************!*\
  !*** ./assets/scripts/containers/ProductsList.jsx ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return ProductsList; });\n/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ \"./node_modules/@babel/runtime/helpers/extends.js\");\n/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/classCallCheck */ \"./node_modules/@babel/runtime/helpers/classCallCheck.js\");\n/* harmony import */ var _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/createClass */ \"./node_modules/@babel/runtime/helpers/createClass.js\");\n/* harmony import */ var _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2__);\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/possibleConstructorReturn */ \"./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js\");\n/* harmony import */ var _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3__);\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime/helpers/getPrototypeOf */ \"./node_modules/@babel/runtime/helpers/getPrototypeOf.js\");\n/* harmony import */ var _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4__);\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @babel/runtime/helpers/inherits */ \"./node_modules/@babel/runtime/helpers/inherits.js\");\n/* harmony import */ var _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ \"./node_modules/react/index.js\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);\n/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react-dom */ \"./node_modules/react-dom/index.js\");\n/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_7__);\n/* harmony import */ var _Product__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./Product */ \"./assets/scripts/containers/Product.jsx\");\n\n\n\n\n\n\n\n\n\n\nvar ProductsList =\n/*#__PURE__*/\nfunction (_React$Component) {\n  _babel_runtime_helpers_inherits__WEBPACK_IMPORTED_MODULE_5___default()(ProductsList, _React$Component);\n\n  function ProductsList() {\n    _babel_runtime_helpers_classCallCheck__WEBPACK_IMPORTED_MODULE_1___default()(this, ProductsList);\n\n    return _babel_runtime_helpers_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_3___default()(this, _babel_runtime_helpers_getPrototypeOf__WEBPACK_IMPORTED_MODULE_4___default()(ProductsList).apply(this, arguments));\n  }\n\n  _babel_runtime_helpers_createClass__WEBPACK_IMPORTED_MODULE_2___default()(ProductsList, [{\n    key: \"render\",\n    value: function render() {\n      this.props.products ? console.log('lista de produtos: ', this.props.products.sort(function (a, b) {\n        return a.price - b.price;\n      })) : '';\n      var products = this.props.products.map(function (product) {\n        return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(_Product__WEBPACK_IMPORTED_MODULE_8__[\"default\"], _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({\n          key: product.id\n        }, product));\n      });\n      return react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"div\", null, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"h1\", {\n        className: \"category-title\"\n      }, this.props.category), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"div\", {\n        className: \"order-options\"\n      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"div\", {\n        className: \"left hide-on-small-only\"\n      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"a\", {\n        href: \"#!\",\n        className: \"active icon-viewmode\"\n      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"i\", {\n        className: \"fas fa-th\"\n      })), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"a\", {\n        href: \"#!\",\n        className: \"icon-viewmode\"\n      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"i\", {\n        className: \"fas fa-th-list\"\n      }))), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"div\", {\n        className: \"right sort-products\"\n      }, \"ordenar por:\", react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"form\", {\n        action: \"\"\n      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(Input, {\n        type: \"select\",\n        label: \"\",\n        defaultValue: \"price\"\n      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"option\", {\n        value: \"price\"\n      }, \"Pre\\xE7o\"))))), react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"div\", {\n        className: \"products\"\n      }, react__WEBPACK_IMPORTED_MODULE_6___default.a.createElement(\"ul\", {\n        className: \"product-list\",\n        itemScope: true,\n        itemType: \"http://schema.org/ItemList\"\n      }, products)));\n    }\n  }]);\n\n  return ProductsList;\n}(react__WEBPACK_IMPORTED_MODULE_6___default.a.Component);\n\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9hc3NldHMvc2NyaXB0cy9jb250YWluZXJzL1Byb2R1Y3RzTGlzdC5qc3guanMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvc2NyaXB0cy9jb250YWluZXJzL1Byb2R1Y3RzTGlzdC5qc3g/MmVhMSJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QgZnJvbSAncmVhY3QnO1xuaW1wb3J0IFJlYWN0RE9NIGZyb20gJ3JlYWN0LWRvbSc7XG5pbXBvcnQgUHJvZHVjdCBmcm9tICcuL1Byb2R1Y3QnO1xuXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIFByb2R1Y3RzTGlzdCBleHRlbmRzIFJlYWN0LkNvbXBvbmVudHtcblx0cmVuZGVyKCl7XG5cdFx0dGhpcy5wcm9wcy5wcm9kdWN0cyA/IGNvbnNvbGUubG9nKCdsaXN0YSBkZSBwcm9kdXRvczogJywgdGhpcy5wcm9wcy5wcm9kdWN0cy5zb3J0KCAoYSxiKSA9PiBhLnByaWNlIC0gYi5wcmljZSkpIDogJycgO1xuXHRcdGxldCBwcm9kdWN0cyA9IHRoaXMucHJvcHMucHJvZHVjdHMubWFwKHByb2R1Y3QgPT4gPFByb2R1Y3Qga2V5PXtwcm9kdWN0LmlkfSB7Li4ucHJvZHVjdH0gLz4pO1xuXHRcdHJldHVybihcblx0XHRcdDxkaXY+XG5cdFx0XHRcdDxoMSBjbGFzc05hbWU9J2NhdGVnb3J5LXRpdGxlJz57dGhpcy5wcm9wcy5jYXRlZ29yeX08L2gxPlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT0nb3JkZXItb3B0aW9ucyc+XG5cdFx0XHRcdFx0PGRpdiBjbGFzc05hbWU9J2xlZnQgaGlkZS1vbi1zbWFsbC1vbmx5Jz5cblx0XHRcdFx0XHRcdDxhIGhyZWY9JyMhJyBjbGFzc05hbWU9J2FjdGl2ZSBpY29uLXZpZXdtb2RlJz48aSBjbGFzc05hbWU9XCJmYXMgZmEtdGhcIj48L2k+PC9hPlxuXHRcdFx0XHRcdFx0PGEgaHJlZj0nIyEnIGNsYXNzTmFtZT0naWNvbi12aWV3bW9kZSc+PGkgY2xhc3NOYW1lPVwiZmFzIGZhLXRoLWxpc3RcIj48L2k+PC9hPlxuXHRcdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHRcdDxkaXYgY2xhc3NOYW1lPSdyaWdodCBzb3J0LXByb2R1Y3RzJz5cblx0XHRcdFx0XHRcdG9yZGVuYXIgcG9yOlxuXHRcdFx0XHRcdFx0PGZvcm0gYWN0aW9uPScnPlxuXHRcdFx0XHRcdFx0PElucHV0IHR5cGU9J3NlbGVjdCcgbGFiZWw9XCJcIiBkZWZhdWx0VmFsdWU9J3ByaWNlJz5cblx0XHRcdFx0XHQgICAgXHQ8b3B0aW9uIHZhbHVlPSdwcmljZSc+UHJlw6dvPC9vcHRpb24+XG5cdFx0XHRcdFx0ICBcdDwvSW5wdXQ+XG5cdFx0XHRcdFx0XHQ8L2Zvcm0+XG5cdFx0XHRcdFx0PC9kaXY+XG5cdFx0XHRcdDwvZGl2PlxuXHRcdFx0XHQ8ZGl2IGNsYXNzTmFtZT0ncHJvZHVjdHMnPlxuXHRcdFx0XHRcdDx1bCBjbGFzc05hbWU9J3Byb2R1Y3QtbGlzdCcgaXRlbVNjb3BlIGl0ZW1UeXBlPVwiaHR0cDovL3NjaGVtYS5vcmcvSXRlbUxpc3RcIj5cblx0XHRcdFx0XHRcdHtwcm9kdWN0c31cblx0XHRcdFx0XHQ8L3VsPlxuXHRcdFx0XHQ8L2Rpdj5cblx0XHRcdDwvZGl2PlxuXHRcdClcblx0fVxufSJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBRUE7Ozs7Ozs7Ozs7Ozs7QUFDQTtBQUNBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQ0E7QUFBQTtBQUNBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRUE7QUFBQTtBQUVBO0FBQUE7QUFDQTtBQUFBO0FBQUE7QUFBQTtBQUNBO0FBQUE7QUFLQTtBQUFBO0FBQ0E7QUFBQTtBQUFBO0FBQUE7QUFNQTs7OztBQTVCQTtBQUNBOyIsInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./assets/scripts/containers/ProductsList.jsx\n");

/***/ }),

/***/ "./node_modules/classnames/index.js":
false,

/***/ "./node_modules/react-materialize/lib/Autocomplete.js":
false,

/***/ "./node_modules/react-materialize/lib/Badge.js":
false,

/***/ "./node_modules/react-materialize/lib/Breadcrumb.js":
false,

/***/ "./node_modules/react-materialize/lib/Button.js":
false,

/***/ "./node_modules/react-materialize/lib/Card.js":
false,

/***/ "./node_modules/react-materialize/lib/CardPanel.js":
false,

/***/ "./node_modules/react-materialize/lib/CardTitle.js":
false,

/***/ "./node_modules/react-materialize/lib/Carousel.js":
false,

/***/ "./node_modules/react-materialize/lib/Chip.js":
false,

/***/ "./node_modules/react-materialize/lib/Col.js":
false,

/***/ "./node_modules/react-materialize/lib/Collapsible.js":
false,

/***/ "./node_modules/react-materialize/lib/CollapsibleItem.js":
false,

/***/ "./node_modules/react-materialize/lib/Collection.js":
false,

/***/ "./node_modules/react-materialize/lib/CollectionItem.js":
false,

/***/ "./node_modules/react-materialize/lib/Container.js":
false,

/***/ "./node_modules/react-materialize/lib/Divider.js":
false,

/***/ "./node_modules/react-materialize/lib/Dropdown.js":
false,

/***/ "./node_modules/react-materialize/lib/Footer.js":
false,

/***/ "./node_modules/react-materialize/lib/Icon.js":
false,

/***/ "./node_modules/react-materialize/lib/Input.js":
false,

/***/ "./node_modules/react-materialize/lib/MediaBox.js":
false,

/***/ "./node_modules/react-materialize/lib/MenuItem.js":
false,

/***/ "./node_modules/react-materialize/lib/Modal.js":
false,

/***/ "./node_modules/react-materialize/lib/NavItem.js":
false,

/***/ "./node_modules/react-materialize/lib/Navbar.js":
false,

/***/ "./node_modules/react-materialize/lib/Pagination.js":
false,

/***/ "./node_modules/react-materialize/lib/PaginationButton.js":
false,

/***/ "./node_modules/react-materialize/lib/Parallax.js":
false,

/***/ "./node_modules/react-materialize/lib/Preloader.js":
false,

/***/ "./node_modules/react-materialize/lib/ProgressBar.js":
false,

/***/ "./node_modules/react-materialize/lib/Row.js":
false,

/***/ "./node_modules/react-materialize/lib/SearchForm.js":
false,

/***/ "./node_modules/react-materialize/lib/Section.js":
false,

/***/ "./node_modules/react-materialize/lib/SideNav.js":
false,

/***/ "./node_modules/react-materialize/lib/SideNavItem.js":
false,

/***/ "./node_modules/react-materialize/lib/Slide.js":
false,

/***/ "./node_modules/react-materialize/lib/Slider.js":
false,

/***/ "./node_modules/react-materialize/lib/Spinner.js":
false,

/***/ "./node_modules/react-materialize/lib/Tab.js":
false,

/***/ "./node_modules/react-materialize/lib/Table.js":
false,

/***/ "./node_modules/react-materialize/lib/Tabs.js":
false,

/***/ "./node_modules/react-materialize/lib/Tag.js":
false,

/***/ "./node_modules/react-materialize/lib/Toast.js":
false,

/***/ "./node_modules/react-materialize/lib/UserView.js":
false,

/***/ "./node_modules/react-materialize/lib/constants.js":
false,

/***/ "./node_modules/react-materialize/lib/idgen.js":
false,

/***/ "./node_modules/react-materialize/lib/index.js":
false

})