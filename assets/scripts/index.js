import React from 'react';
import ReactDOM from 'react-dom';
import css from '../style/base.scss';
import Home from './containers/Home';

const title = 'My Minimal React Webpack Babel Setup';

ReactDOM.render(
  <Home />,
  document.getElementById('main')
);

module.hot.accept();
