import React from 'react';
import ReactDOM from 'react-dom';
import NumberFormat from 'react-number-format';

import CONST from '../constants';
let url = CONST.MAIN_URL;

export default class Product extends React.Component{

	productImage(){
		if(this.props.image){
		  return(<div className="product-image">
		    <img src={`${url}${this.props.image}`} itemProp="image" alt={this.props.name} />
		  </div>);
		}
	}

	render(){
		return(
			<li itemProp="itemListElement" itemScope itemType="http://schema.org/Product">
				{this.productImage()}
				<div itemProp="name" className='product-name'>{this.props.name}</div>
				<div itemProp="price" className='product-price'>
					<NumberFormat 
						value={this.props.price} 
						displayType={'text'} 
						decimalSeparator={','} 
						thousandSeparator={'.'} 
						prefix={'R$'} 
						fixedDecimalScale={true} 
						decimalScale={2}
					/>
				</div>
				<div className='prodcut-action'>
					<a href={`${url}${this.props.path}`} itemProp="url" className='btn btn-primary'>Comprar</a>
				</div>
			</li>
		)
	}

}