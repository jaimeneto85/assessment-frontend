import React from 'react';
import ReactDOM from 'react-dom';
import queryString  from 'query-string';

import API from '../API';

// stores
import webjumpStore from '../stores/webjumpStore';

// actions
import webjumpActions from '../actions/webjumpActions';

// components
import ProductsList from './ProductsList';
import Filter from './Filter';

let getAppState = () => {
  return { productList: webjumpStore.getAll(), categoryList: webjumpStore.getCategories()}
}

let category_id = 0;
export default class Home extends React.Component{

	constructor(props){
    	super(props);
    	this._getProducts();
    	this._getCategories();
    	this.state = getAppState();
    	this._onChange = this._onChange.bind(this);
    	if(location.search){
    		var queryParsed = queryString.parse(location.search);
    		if(queryParsed.categoria){
    			category_id = queryParsed.categoria;
    			this._getProducts(queryParsed.categoria)
    		}
    	}
	}

	componentDidMount(){
		webjumpStore.addChangeListener(this._onChange);
	}

	componentWillUnmount(){
		webjumpStore.removeChangeListener(this._onChange);
	}

	componentWillReceiveProps(props){
		this._getPage(props.location.search);
	}

	_onChange(){
		this.setState({...getAppState()});
		this._getCategory(category_id);
		
	}

	_getCategory( category ){
		if(category && this.state.categoryList){
			let categorySelected = this.state.categoryList.items.find(cat => cat.id == category )
			this.setState({category: categorySelected.name })
		} else {
			this.setState({category: 'Produtos' })
		}

	}

	_getProducts( categoria = 3 ){
		webjumpActions.getProducts( categoria );
	}

	_getCategories(){
		webjumpActions.getCategories();
	}


	render(){
		return(
			<div className='container'>
				<div className='row'>
					<div className='col s12 breadcrumb-container'>
				        <a href="#!" className="breadcrumb">Página inicial </a> > 
				        <span className='breadcrumb'> {this.state.category}</span>
					</div>
					<div className='col s12 m2 l2'>
						<Filter filters={this.state.productList.filters} categories={this.state.categoryList.items} />
					</div>
					<div className='col s12 m10 l10 product-list-container'>
						<ProductsList products={this.state.productList.items} categories={this.state.categoryList.items} category={this.state.category} />
					</div>
				</div>
			</div>
		)
	}

}