import React from 'react';
import ReactDOM from 'react-dom';
import Product from './Product';

export default class ProductsList extends React.Component{
	render(){
		this.props.products ? console.log('lista de produtos: ', this.props.products.sort( (a,b) => a.price - b.price)) : '' ;
		let products = this.props.products.map(product => <Product key={product.id} {...product} />);
		return(
			<div>
				<h1 className='category-title'>{this.props.category}</h1>
				<div className='order-options'>
					<div className='left hide-on-small-only'>
						<a href='#!' className='active icon-viewmode'><i className="fas fa-th"></i></a>
						<a href='#!' className='icon-viewmode'><i className="fas fa-th-list"></i></a>
					</div>
					<div className='right sort-products'>
						<form action=''>
							<label htmlFor='sort'>ordernar por:</label>
							<select name='sort' id='sort' className='select-sort'>
								<option value='relevance'>Relevância</option>
								<option value='price'>Preço</option>
							</select>
						</form>
					</div>
				</div>
				<div className='products'>
					<ul className='product-list' itemScope itemType="http://schema.org/ItemList">
						{products}
					</ul>
				</div>
			</div>
		)
	}
}