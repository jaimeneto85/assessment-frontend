import React from 'react';
import ReactDOM from 'react-dom';

export default class Filter extends React.Component{

	render(){
		let categories = [];
		if(this.props && this.props.categories){
			categories = this.props.categories.map(category => {return (<li key={category.id} {...category} className='filter-option'><a href={`/?categoria=${category.id}`} >{category.name}</a></li>)});
    	}
		return(
			<div className='filter-block'>
				<h2 className='filter-title'>Filtre por </h2>
				
				<div className='filter-list'>
					<ul className='filter-item'>
						<div className='filter-item-title'>Categorias</div>
						{categories}
					</ul>
				</div>
			</div>
		)
	}
}