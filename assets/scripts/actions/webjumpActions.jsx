import API from '../API';

export default{
  getProducts(categoria){
    API.getProducts(categoria);
  },

  getCategories(){
  	API.getCategories();
  }

}
