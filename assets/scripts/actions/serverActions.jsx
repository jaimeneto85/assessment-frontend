import AppDispatcher from '../dispatcher';
import ActionTypes from '../constants';

export default {
  receivedProducts(rawProducts, append){
    AppDispatcher.dispatch({
      actionType: ActionTypes.RECEIVED_PRODUCTS,
      rawProducts, append
    })
  },

  receivedCategories(rawCategories){
  	AppDispatcher.dispatch({
  		actionType: ActionTypes.RECEIVED_CATEGORIES,
  		rawCategories
  	})
  }
}
