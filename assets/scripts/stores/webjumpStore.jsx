import AppDispatcher from '../dispatcher';
import ActionTypes from '../constants';
import AppEventEmitter from './appEventEmitter';


let _products = {items: [], filters: []};
let _categories = {items: [] };


class millbodyEventEmitter extends AppEventEmitter{
  getAll(){
    return _products;
  }

  getCategories(){
    return _categories;
  }
}

let webjumpStore = new millbodyEventEmitter();

AppDispatcher.register( action => {
  switch(action.actionType){
    case ActionTypes.RECEIVED_PRODUCTS:
      action.append && _products.length > 0 ? _products = _products.concat(action.rawProducts) : _products = action.rawProducts;
      webjumpStore.emitChange();
      break;
    case ActionTypes.RECEIVED_A_PRODUCT:
      _products.unshift(action.product);
      webjumpStore.emitChange();
      break;
    case ActionTypes.RECEIVED_CATEGORIES:
      action.append && _categories.length > 0 ? _categories = _categories.concat(action.rawCategories) : _categories = action.rawCategories;
      webjumpStore.emitChange();
      break;
    default:
      break;
  }
})

export default webjumpStore;
