import CONST from './constants';
import ServerActions from './actions/ServerActions';

let url = CONST.URL;
export default{
	getProducts(categoria){
		fetch(`${url}categories/${categoria}`)
		.then(res => res.json() )
	    .then(resposta => { ServerActions.receivedProducts(resposta) } )
	    .catch(err => console.log(err))
	},

	getCategories(){
		fetch(`${url}categories/list`)
		.then(res => res.json())
		.then(resposta => { ServerActions.receivedCategories(resposta) })
	}
}